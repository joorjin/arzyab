<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>فرم ارزیابی</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css"
          integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">

    <style>
        body {
            overflow-x: hidden;
        }

        .fa-text {
            direction: rtl
        }
        .custom-card {
            visibility: visible;
            opacity: 1;
            height: auto;
        }
        .d-none {
            visibility: hidden;
            opacity: 0;
            height: 0;
        }
    </style>
</head>

<body>
<br>
<h3 style="direction: rtl;"> با دقت به سوالات پاسخ دهید</h3>
<br>
<div class="row" >
    @if ($errors->any())
        <div class="alert alert-danger text-right">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>

        </div>
    @endif

    @if(isset($_GET['organization']))
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="row">
                <form method="post" action="/respons" class="row">
                    @csrf
                    @php(\App\Http\Helpers\ViewQuestionBuilder::build($questions, true, 0, $response))

                    <button type="submit" class="btn mt-5 btn-success"> ثبت</button>

                </form>
                <div style="height: 2rem"></div>
            </div>
        </div>
    @else
        @foreach(\App\Models\User::find(\Illuminate\Support\Facades\Auth::id())->organizations()->get()->toArray() as $index => $organization)
                <div class="col-md-8 m-auto" style="direction: rtl">
                   {{ $index + 1 }} - <a href="?organization={{ $organization['id'] }}"> {{ $organization['organization']['name'] }} </a>
                    <hr>
                </div>
        @endforeach
    @endif


</div>
. <br>
</body>
<script>
    function ActiveSub(e)
    {
        const parent = e.parentNode.parentNode.parentNode
        const stage = +parent.getAttribute('aria-current') + 1
        const children = parent.querySelectorAll('div[aria-current="'+ (stage) +'"]')
        children.forEach(function (item){
            item.classList.remove('d-none')
        })
    }
    function inactiveSub(e)
    {
        const parent = e.parentNode.parentNode.parentNode
        const stage = +parent.getAttribute('aria-current') + 1
        const children = parent.querySelectorAll('div[aria-current="'+ (stage) +'"]')
        children.forEach(function (item){
            item.classList.add('d-none')
        })
    }
</script>
</html>
