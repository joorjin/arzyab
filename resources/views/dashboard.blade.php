@extends('layouts.master')

@section('content')
<style>
    .question-box{
        margin: 0 auto;
    }
    .question-box .en-text{
        text-align: left;
    }
</style>

@if ($errors->any())
<div class="alert alert-danger text-right">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>

</div>
@endif

@if(isset($_GET['organization']))


    <div class="row" >
        <form method="post" action="/respons" style="display: block;width: 100%;" >
            @csrf
            @php(\App\Http\Helpers\ViewQuestionBuilder::build($questions, true, 0, $response))

            <button type="submit" class="btn mt-5 btn-success"> ثبت</button>

        </form>
        <div style="height: 2rem"></div>
    </div>
@else
<p class="text-secondary">  نام سازمانی که میخواهید در ارزیابی آن شرکت کنید </p>

@foreach(\App\Models\User::find(\Illuminate\Support\Facades\Auth::id())->organizations()->get()->toArray() as $index => $organization)
        <div class="col-md-8 m-auto" style="direction: rtl">
           {{ $index + 1 }} - <a href="?organization={{ $organization['id'] }}"> {{ $organization['organization']['name'] }} </a>
            <hr>
        </div>
@endforeach
@endif



<script>
    $(document).ready(function () {

        $('.checkbox').click(function (e) {
            var checkbox =  $('.checkbox input:checked').val();
            if (checkbox == '0') {
                var  question_id = $(this).attr('question-id');
                $("[parent-id="+question_id+"]").hide();

            }

            if (checkbox == '1') {
                var  question_id = $(this).attr('question-id');
                $("[parent-id="+question_id+"]").show();

            }
        });

    });
</script>

@endsection
{{-- d-none --}}
