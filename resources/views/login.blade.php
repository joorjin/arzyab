<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>login</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css"
          integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">

    <style>
        body {
            direction: rtl;
        }
    </style>
</head>

<body style="overflow-x: hidden;">
<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6 card" style="top: 100px">
        @if ($errors->any())
            <div class="alert alert-danger text-right">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>

            </div>
        @endif
        <form method="POST" action="{{ route('verify_code') }}">
            @csrf
            <br>
            <br>
            <!-- Phone input -->
            <div class="form-outline mb-4">
                <input type="text" id="phonenumber" class="form-control" name="phone"/>
                <label class="form-label" for="phonenumber"> شماره تلفن</label>
            </div>

            <!-- Submit button -->
            <button type="submit" class="btn btn-primary btn-block mb-4">ورود</button>

            <!-- Register buttons -->

        </form>
    </div>
</div>
</body>

</html>
