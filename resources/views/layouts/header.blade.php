<!doctype html>
<html lang="en">
<head>
    <title>قالب ادمین فارسی | صفحه اصلی</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.3.1/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/box-statistics.css">
    <link rel="stylesheet" href="css/calendar.css">
    <link rel="stylesheet" href="css/persian-datepicker.min.css">
    <script src="js/jquery.min.js"></script>


    <style>
        body{
            max-width: 2000px;
            margin: 0 auto;
        }
    </style>
</head>
<body dir="rtl">

<div class="wrapper d-flex align-items-stretch">
    <nav id="sidebar" >

        <ul class="list-unstyled position-fixed components mb-5 mt-5">
            <li class="active">
                <a href="/dashboard"><span class="fa fa-home"></span> داشبورد </a>
            </li>
            {{-- <li>
                <a href="#"><span class="fa fa-user"></span> کاربرها </a>
            </li>
            <li>
                <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false"> <span
                        class="fa fa-sticky-note"></span> وبلاگ <i class='fa fa-angle-down arrow-menu'></i> </a>
                <ul class="collapse list-unstyled" id="homeSubmenu">
                    <li>
                        <a href="#">صفحه 1</a>
                    </li>
                    <li>
                        <a href="#">صفحه 2</a>
                    </li>
                    <li>
                        <a href="#">صفحه 3</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#"><span class="fa fa-cogs"></span> خدمات </a>
            </li>
            <li>
                <a href="#"><span class="fa fa-paper-plane"></span> ارتباط با ما </a>
            </li> --}}
        </ul>
    </nav>

    <!-- Page Content  -->
    <div id="content" class="p-4 p-md-5">

        <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #e3f2fd;">
            <div class="container-fluid">

                <button type="button" id="sidebarCollapse" class="btn btn-primary">
                    <i class="fa fa-bars"></i>
                    <span class="sr-only">منو</span>
                </button>


                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="nav navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">صفحه اصلی</a>
                        </li>
                    </ul>
                    <div class="my-2 my-lg-0">
                        <b> {{ \App\Models\User::find(\Illuminate\Support\Facades\Auth::id())->first()->name  }} </b>
                    </div>
                </div>
            </div>
        </nav>
