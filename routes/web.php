<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use Illuminate\Support\Facades\Route;


Route::view('/', 'index');
Route   ::get('/login', 			[AuthController::class, 'index'])		->name('login');
Route::post('/validation',		[AuthController::class, 'verifyCode'])	->name('verify_code');
Route::get('/validation',		[AuthController::class, 'verifyCodeView'])->name('view_verify_code');
Route::post('/post_login',		[LoginController::class, 'login'])		->name('post_login');

Route::middleware('auth')->group(function () {
    Route::get('/dashboard',	[DashboardController::class, 'dashboard'])->name('dashboard');
    Route::post('/respons', 	[DashboardController::class, 'respons'])->name('respons');
});


