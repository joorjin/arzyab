<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasFactory;

	protected $guarded = [];

	public function organizations(): HasMany
	{
		return $this->hasMany(UserOrganization::class, 'user_id', 'id')->with('organization');
	}

}
