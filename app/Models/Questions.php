<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Questions extends Model
{
    use HasFactory;

	protected $guarded = [];

	public function children(): HasMany
	{
		return $this->hasMany(Questions::class, 'parent_id', 'id')
			->with('children');
	}
}
