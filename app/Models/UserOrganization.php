<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class UserOrganization extends Model
{
    use HasFactory;

	protected $guarded = [];


	public function organization(): HasOne
	{
		return $this->hasOne(Organization::class, 'id', 'organization_id');
	}
}
