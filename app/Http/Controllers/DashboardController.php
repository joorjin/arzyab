<?php

namespace App\Http\Controllers;

use App\Models\AccessQuestion;
use App\Models\Questions;
use App\Models\Responses;
use App\Models\UserOrganization;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function dashboard(Request $request)
	{
		if (isset($request->organization)){
			$organization =  $request->organization;
		}
		$userid = Auth::id();
        $AccessibleQuestionsID = UserOrganization::where('user_id',$userid)
			->rightJoin('access_questions','user_organizations.id','access_questions.user_organization_id')
			->rightJoin('questions','access_questions.question_id','questions.id')
			->whereNull('parent_id')
			->where('user_organizations.id', $organization ?? 0)
			->pluck('questions.id')->toArray();

		$main_questions = Questions::with('children')
			->whereIn('id', $AccessibleQuestionsID)->get();

        $responses = UserOrganization::
        	where('user_id',$userid)
        ->rightJoin('responses','user_organizations.id','responses.user_organization_id')
//        ->select('question_id')
        ->get()->toArray();

		$res = [];
		foreach ($responses as $response) {
			$res[$response['question_id']] = $response;
		}

        return view('dashboard',[
            'questions' => isset($main_questions) ? $main_questions->toArray() : [],
			'response'	=> $res
        ]);

    }


    public function respons(Request $request): \Illuminate\Http\RedirectResponse
	{
		$userid = Auth::id();

		$result = [];

		foreach ($request->all()['answer'] as $field){
			if (!is_array($field)) continue;
			if (!isset($field['bool'])) continue;
			if ($field['disabled']) continue;
			$field['user_organization_id'] = $userid;
			$result[] = $field;
		}

        Responses::insert($result);

        return redirect()->route('dashboard');
    }




    private function getBoolResponse($question_id){
        $respons =  Responses::where('question_id',$question_id)->first();

        if ($respons != null  ) {
            return $respons->bool;
        }else{
            return false;
        }
    }

}
