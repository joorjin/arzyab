<?php

namespace App\Http\Controllers;

use App\Http\Helpers\SmsKavehNagar;
use App\Models\User;
use App\Models\UserToken;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    public function index(Request $request): Factory|View|RedirectResponse|Application
	{
        return view('login');
    }

	public function verifyCode(Request $request): RedirectResponse
	{
		$request->validate([
			'phone' => 'required'
		]);

		$user = User::where('phone',$request->phone)->first();
		if (!$user) {
			return Redirect::back()->withErrors('شماره وجود ندارد');
		}

//		$tempPwd = rand(9999, 99999);
		$tempPwd = 1234;

		$user->update([
			'password' => Hash::make($tempPwd),
		]);

//		SmsKavehNagar::send($tempPwd, $user->phone);

		return \redirect()->route('view_verify_code')->with([
			'code' 	=> true,
			'phone'	=> $user->phone
		]);
	}

	public function verifyCodeView(): Factory|View|Application
	{
		$phone = Session::get('phone');
		return view('verify', compact('phone'));
	}
}
