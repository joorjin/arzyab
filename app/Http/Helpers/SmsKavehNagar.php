<?php

namespace App\Http\Helpers;

use Illuminate\Support\Facades\Config;
use Kavenegar\KavenegarApi;

class SmsKavehNagar
{
	public static function send(int $code, $receptor): void
	{
		(new KavenegarApi(Config::get('kavenegar.apikey')))->Send(null, $receptor, 'کد شما جهت ورود در سامانه : ' . $code);
	}
}
