<?php

namespace App\Http\Helpers;

trait ViewQuestionBuilder
{


	public static function build($questions, bool $visibility = true, $stage = 0, $response = []): void
	{

		if (!is_array($questions)){
			$questions = $questions->toArray();
		}
		foreach($questions as $question) {
			$disabled = false;
			if (isset($response[$question['id']]['bool']))
				$childVisibility = $response[$question['id']]['bool'];
			else
				$childVisibility = $visibility;

			if (isset($response[$question['id']])){
				$disabled = true;
			}


			print  '<div parent-id="' . $question['parent_id'] . '"   class="col-md-6 card mt-2 p-2 custom-card question-box'. (!$visibility ? 'd-none' : null) .'" aria-current="'. $stage .'" style="max-width:100%;">
                        <div class="fa-text text-darck">
                            <p>
                                ' . $question['title_fa'] . '
                            </p>
                        </div>
                        <div class="en-text text-darck">
                            <p>
                                ' . $question['title_en'] . '

                            </p>
                        </div>
                        <br>
                        <br>
                        <br>
                        <div class="fa-text text-darck">
                            <p>
                                ' . $question['description_fa'] . '

                            </p>
                        </div>
                        <div class="en-text text-darck">
                            <p>
                                ' . $question['description_en'] . '

                            </p>
                        </div>

                        <div class="col-md-3 m-auto checkbox" question-id="' . $question['id'] . '" style="text-align: right; max-width:100%;">
                            <div class="form-check">
                                <input  class="form-check-input" style="position:relative;margin-left:0;" type="radio" name="answer[' . $question['id'] . '][bool]"
                                 onclick="ActiveSub(this)"
                                 id="exampleRadios2"
                                       value="1" '. (@$response[$question['id']]['bool'] ? 'checked' : '' ) .'>
                                <label class="form-check-label" for="exampleRadios2">
	بله
                                </label>
                            </div>
                            <div class="form-check disabled">
                                <input  class="form-check-input" style="position:relative;margin-left:0;" type="radio" name="answer[' . $question['id'] . '][bool]" id="exampleRadios3"
                                onclick="inactiveSub(this)"
                                       value="0" '. (@!$response[$question['id']]['bool'] ? ((isset($response[$question['id']]['bool']) && !is_null($response[$question['id']]['bool'])) ? 'checked' : '') : '' ) .'>
                                <label class="form-check-label" for="exampleRadios3">
	خیر
                                </label>
                            </div>
                        </div>

                        <input type="hidden" name="answer[' . $question['id'] . '][question_id]" value="' . $question['id'] . '">
                        <input type="hidden" name="answer[' . $question['id'] . '][disabled]" value="' . $disabled . '">
                        <div class="col-md-5 form-group" style="text-align: right;">
                            <label for="ex1">چند درصد؟</label>
                            <input type="number" id="ex1" class="form-control" style="direction: rtl;"
                                   name="answer[' . $question['id'] . '][percent]" min="0" max="100" value="'. @$response[$question['id']]['percent'] .'">
                        </div>
                        <div class="col-md-3"></div>
                        <br>
                        <br>
                        <br>
                        <div class="col-md-12">
                            <div class="form-group" style="direction: rtl;">
                                <label for="exampleFormControlTextarea1"> توضیحات</label>
                                <textarea name="answer[' . $question['id'] . '][description]" class="form-control" id="exampleFormControlTextarea1"
                                          rows="3">'. @$response[$question['id']]['description'] .'</textarea>
                            </div>
                        </div>
                        <br>
                        <br>
                        <hr>

						';

			print self::build($question['children'], $childVisibility, ++$stage, $response) .'
				</div>
		';
		}

	}
}
